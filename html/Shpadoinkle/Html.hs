module Shpadoinkle.Html
  ( module Shpadoinkle.Html.Element
  , module Shpadoinkle.Html.Property
  , module Shpadoinkle.Html.Event
  , module Shpadoinkle.Html.TH
  , module Shpadoinkle
  ) where


import           Shpadoinkle               hiding (Shpadoinkle, shpadoinkle)
import           Shpadoinkle.Html.Element
import           Shpadoinkle.Html.Event
import           Shpadoinkle.Html.Property
import           Shpadoinkle.Html.TH
